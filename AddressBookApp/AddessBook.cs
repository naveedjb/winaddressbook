﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddressBookApp
{
    public partial class AddessBook : Form
    {

     
       
       
      

        public AddessBook()
        {
            InitializeComponent();
        }
      
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Are you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    AddressBook.list.WriteList();
                    break;
            }
        }
       


        private void AddressForm_ButtonClicked(object sender,
                            AddressUpdateEventArgs e)
        {
            // update the forms values from the event args
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = e.list;
        
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           dataGridView1.DataSource= AddressBook.ListAllMenu();
        }

       
       

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

       

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string val = "";
            EditForm editForm = new EditForm();
            editForm.AddressUpdated += new EditForm.AddressUpdateHandler(AddressForm_ButtonClicked);

            
            if (e.RowIndex >= 0)
             {

            DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                string name = row.Cells[0].Value.ToString();
                string email = row.Cells[1].Value.ToString();
                string phone = row.Cells[2].Value.ToString();
             //   val += name + email + phone;
                //...
                editForm.email = email;
                editForm.NameTxt.Text = name;
                editForm.EmailTxt.Text = email;
                editForm.PhoneTxt.Text = phone;
                editForm.Show();
            }
            
            //MessageBox.Show("Hello "+val);
        }

       

        private void SearchTxt_KeyUp(object sender, KeyEventArgs e)
        {
            if(!AddressUtility.checkInput(e.KeyValue)){
                var data=SearchTxt.Text;
            var list=AddressBook.SearchMenu(data);
            dataGridView1.DataSource = list;
            }
        }

        private void AddContactBtn_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();

            addForm.AddressUpdated += new AddForm
       .AddressUpdateHandler(AddressForm_ButtonClicked);
            addForm.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    
    
    
    
    }
}
