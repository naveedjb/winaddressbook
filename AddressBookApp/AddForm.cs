﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddressBookApp
{
    public partial class AddForm : Form
    {
        public delegate void AddressUpdateHandler(
            object sender, AddressUpdateEventArgs e);

        // add an event of the delegate type
        public event AddressUpdateHandler AddressUpdated;

        public AddForm()
        {
            InitializeComponent();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var name = AddressUtility.checkName( NameTxt.Text);
                var email = AddressUtility.checkEmail( EmailTxt.Text);
                var phone = AddressUtility.checkPhone( PhoneNoTxt.Text);
                IContactModel cm = new ContactModel();
                cm.Name = name;
                cm.Email = email;
                cm.Phone = phone;
                AddressBook.AddItemMenu(cm);
                AddressUpdateEventArgs args =
        new AddressUpdateEventArgs(AddressBook.ListAllMenu());

                // raise the event with the updated arguments

                this.AddressUpdated(this, args);

                this.Hide();
            }
            catch (AddressException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void AddForm_Load(object sender, EventArgs e)
        {

        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
