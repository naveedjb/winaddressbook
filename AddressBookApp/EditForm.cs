﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddressBookApp
{
    public partial class EditForm : Form
    {
        public delegate void AddressUpdateHandler(
            object sender, AddressUpdateEventArgs e);

        // add an event of the delegate type
        public event AddressUpdateHandler AddressUpdated;
        public string email = "";
        public EditForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
              //  email = AddressUtility.checkEmail(searchTxt.Text);
              //  email = AddressUtility.checkEmail("");
                IContactModel model = AddressBook.GetItemByEmail(email);
                if (model == null)
                    throw new AddressException("No Record found");
                NameTxt.Text = model.Name;
                EmailTxt.Text = model.Email;
                PhoneTxt.Text = model.Phone;


                EditPanel.Visible = true;
            }
            catch (AddressException ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                IContactModel model = new ContactModel();
                model.Name = AddressUtility.checkName(NameTxt.Text);
                model.Email = AddressUtility.checkEmail(EmailTxt.Text);
                model.Phone = AddressUtility.checkPhone(PhoneTxt.Text);
                AddressBook.EditItemMenu(email, model);
                AddressUpdateEventArgs args =
            new AddressUpdateEventArgs(AddressBook.ListAllMenu());

                // raise the event with the updated arguments

                this.AddressUpdated(this, args);
                this.Close();
            }
            catch (AddressException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            AddressBook.DeleteItemMenu(email);
            AddressUpdateEventArgs args =
        new AddressUpdateEventArgs(AddressBook.ListAllMenu());

            // raise the event with the updated arguments

            this.AddressUpdated(this, args);
            this.Close();
        }


    }
}
